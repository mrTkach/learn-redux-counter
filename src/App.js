import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {increment, decrement} from './actions';

const App = () => {
   const [inputValue, setInputValue] = useState(0);
   const counter = useSelector(state => state.counter);
   const dispatch = useDispatch();

   return (
      <div style={{textAlign: 'center'}}>
         <div>Counter {counter}</div>
         <button onClick={ () => dispatch(decrement(inputValue)) }>-</button>
         <button onClick={ () => dispatch(increment(inputValue)) }>+</button>
      </div>
   );
}

export default App;
