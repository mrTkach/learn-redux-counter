import {combineReducers} from "redux";

import counterReducer from './counter-reducer';
import loggedReducer from './isLogged-reducer';

const reducers = combineReducers({
   counter: counterReducer,
   isLogged: loggedReducer
})

export default reducers;
